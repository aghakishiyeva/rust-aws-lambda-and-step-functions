use lambda_runtime::{handler_fn, Context, Error};
use serde_json::{Value, json};

async fn function_handler(event: Value, _: Context) -> Result<Value, Error> {
    // Example: Transform the input by adding a new field or modifying existing ones
    let mut transformed_data = event;
    if let Some(obj) = transformed_data.as_object_mut() {
        obj.insert("processed".to_string(), json!("true"));
    }

    Ok(json!({ "message": "Data processed", "output": transformed_data }))
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(function_handler);
    lambda_runtime::run(func).await?;
    Ok(())
}
