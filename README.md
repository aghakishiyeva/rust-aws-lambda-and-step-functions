# Rust AWS Lambda and Step Functions

## Overview
This project demonstrates the integration of Rust AWS Lambda functions with AWS Step Functions for orchestrating a data processing pipeline.

## Project Structure
- `src/main.rs`: Contains the Rust Lambda function code.
- `Cargo.toml`: Rust project configuration file.
- `state_machine.json`: Definition file for the AWS Step Functions state machine.

## Setup
1. **Rust Lambda Function**: Implement your Rust Lambda function in `src/main.rs`. This function should handle the processing logic.
2. **Build and Deploy Lambda Function**: Build your Rust Lambda function using Cargo, then package and deploy it to AWS Lambda.
3. **Step Functions Workflow**: Define the Step Functions workflow using the Amazon States Language (ASL). Save the definition in `state_machine.json`.
4. **Deploy State Machine**: Use AWS CLI to deploy the state machine defined in `state_machine.json` to AWS Step Functions.

## Usage
1. **Invoke Lambda Function**: Test your Lambda function by invoking it using the AWS CLI:
    ```
    aws lambda invoke --function-name rustLambdaFunction --payload '{"test":"data"}' output.json
    ```
   Replace `rustLambdaFunction` with the actual name of your Lambda function.

2. **Start Step Functions Execution**: Start an execution of the Step Functions state machine:
    ```
    aws stepfunctions start-execution --state-machine-arn "arn:aws:states:YOUR_REGION:YOUR_ACCOUNT_ID:stateMachine:YOUR_STATE_MACHINE_NAME" --input '{"test":"data"}'
    ```